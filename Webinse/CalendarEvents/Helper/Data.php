<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 12:58 PM
 */
namespace Webinse\CalendarEvents\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\Mail\Template\TransportBuilder;
use \Magento\Framework\Translate\Inline\StateInterface;
use \Magento\Framework\App\ObjectManager;
class Data extends AbstractHelper
{
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @param Context $context
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation
    ) {
        parent::__construct($context);
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
    }

    private function _getConfig($config_path)
    {
        $config = $this->scopeConfig
            ->getValue($config_path, ScopeInterface::SCOPE_STORE);

        return $config;
    }

    /** Calendar style getters **/

    public function getNextMon()
    {
        return $this->_getConfig('calendar_events/events/next_mon');
    }

    public function getPrevMon()
    {
        return $this->_getConfig('calendar_events/events/prev_mon');
    }

    public function getEventColor()
    {
        return $this->_getConfig('calendar_events/calendar_design/event_color');
    }

    public function getEventTextColor()
    {
        return $this->_getConfig('calendar_events/calendar_design/event_text_color');
    }

    public function getEventBorderColor()
    {
        return $this->_getConfig('calendar_events/calendar_design/event_border_color');
    }

    public function getFirstDay()
    {
        return $this->_getConfig('calendar_events/events/first_day');
    }

    public function getDayName()
    {
        return $this->_getConfig('calendar_events/events/day_name');
    }

    public function getEventTextSize()
    {
        return $this->_getConfig('calendar_events/calendar_design/event_text_size');
    }

    public function getEventTextTransform()
    {
        return $this->_getConfig('calendar_events/calendar_design/event_text_transform');
    }

    public function getEventFontFamily()
    {
        return $this->_getConfig('calendar_events/calendar_design/event_font_family');
    }

    public function getEventFontStyle()
    {
        return $this->_getConfig('calendar_events/calendar_design/event_font_style');
    }

    /** Email info getters **/
    public function getEmailTemplate()
    {
        return $this->_getConfig('calendar_events/notifications/email_template');
    }

    public function getSender()
    {
        return $this->_getConfig('calendar_events/notifications/sender');
    }

    public function getEmailSubject()
    {
        return $this->_getConfig('calendar_events/notifications/email_subject');
    }

    public function getRecipientName()
    {
        return $this->_getConfig('calendar_events/notifications/recipient_name');
    }

    public function getRecipientEmail()
    {
        return $this->_getConfig('calendar_events/notifications/recipient_email');
    }

    /**
     * Send transactional email
     * at admin save calendar configs
     * @param $block
     * @param $objectNew
     */
    public function sendMail($block, $objectNew)
    {
        $this->inlineTranslation->suspend();
        try {
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($block);

            $sender = $this->getSender();
            $objectManager = ObjectManager::getInstance();
            $urlBuilder = $objectManager->create('Magento\Framework\UrlInterface');
            if($objectNew){
                $pageStatus = 'created';
            }
            else{
                $pageStatus = 'edited';
            }
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier($this->getEmailTemplate())
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'subject'    => $this->getEmailSubject(),
                    'pageId'     => $block['id'],
                    'pageTitle'  => $block['title'],
                    'link'       => $urlBuilder->getBaseUrl().'calendar_events',
                    'pageStatus' => $pageStatus,
                    'pageUrl'    => $urlBuilder->getBaseUrl().'calendar_events/show/event/id/'.$block['id']
                ])
                ->setFrom($sender)
                ->addTo($this->getRecipientEmail())
                ->getTransport();

            $transport->sendMessage(); ;
            $this->inlineTranslation->resume();

            return;
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();

            return;
        }
    }
}