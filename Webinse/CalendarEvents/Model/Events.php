<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 5:52 PM
 */
namespace Webinse\CalendarEvents\Model;

use Magento\Framework\Model\AbstractModel;

class Events extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Webinse\CalendarEvents\Model\Resource\Events');
    }
}