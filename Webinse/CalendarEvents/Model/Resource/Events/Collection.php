<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 5:59 PM
 */

namespace Webinse\CalendarEvents\Model\Resource\Events;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Webinse\CalendarEvents\Model\Events',
            'Webinse\CalendarEvents\Model\Resource\Events'
        );
    }
}