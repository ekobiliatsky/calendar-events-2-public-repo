<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 5:55 PM
 */
namespace Webinse\CalendarEvents\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Events extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('webinse_calendarevents', 'id');
    }
}