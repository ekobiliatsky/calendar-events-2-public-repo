<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/16/16
 * Time: 4:54 PM
 */
namespace Webinse\CalendarEvents\Model\Config\Source;
use \Magento\Framework\Option\ArrayInterface;

class WidgetView implements ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'basicDay', 'label' => __('Day')],
            ['value' => 'basicWeek', 'label' => __('Week')],
            ['value' => 'month', 'label' => __('Month')],
        ];
    }
}