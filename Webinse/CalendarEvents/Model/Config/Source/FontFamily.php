<?php
/**
 * Copyright © 2016 Webinse. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webinse\CalendarEvents\Model\Config\Source;
use \Magento\Framework\Option\ArrayInterface;

class FontFamily implements ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'serif', 'label' => __('Serif')],
            ['value' => 'sans-serif', 'label' => __('Sans-Serif')],
            ['value' => 'monospace', 'label' =>__('Monospace')],
        ];
    }
}
