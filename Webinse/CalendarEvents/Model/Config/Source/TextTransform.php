<?php
/**
 * Copyright © 2016 Webinse. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webinse\CalendarEvents\Model\Config\Source;
use \Magento\Framework\Option\ArrayInterface;
class TextTransform implements ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'uppercase', 'label' => __('Uppercase')],
            ['value' => 'lowercase', 'label' => __('Lowercase')],
            ['value' => 'capitalize', 'label' => __('Capitalize')],
        ];
    }
}
