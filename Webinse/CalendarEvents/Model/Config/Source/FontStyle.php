<?php
/**
 * Copyright © 2016 Webinse. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webinse\CalendarEvents\Model\Config\Source;
use \Magento\Framework\Option\ArrayInterface;
class FontStyle implements ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'normal', 'label' => __('Normal')],
            ['value' => 'italic', 'label' => __('Italic')],
        ];
    }
}
