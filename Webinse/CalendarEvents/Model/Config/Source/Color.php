<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/16/16
 * Time: 4:54 PM
 */
namespace Webinse\CalendarEvents\Model\Config\Source;
use \Magento\Framework\Option\ArrayInterface;

class Color implements ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'inherit', 'label' => __('Inherit from main calendar')],
            ['value' => '000000', 'label' => __('Black')],
            ['value' => '0000ff', 'label' => __('Blue')],
            ['value' => '8b2323', 'label' => __('Brown')],
            ['value' => 'bebebe', 'label' => __('Gray')],
            ['value' => '00cd00', 'label' => __('Green')],
            ['value' => 'e6e6fa', 'label' => __('Lavender')],
            ['value' => 'b03060', 'label' => __('Maroon')],
            ['value' => 'ee9a00', 'label' => __('Orange')],
            ['value' => 'a020f0', 'label' => __('Purple')],
            ['value' => 'cd0000', 'label' => __('Red')],
            ['value' => 'ffffff', 'label' => __('White')],
            ['value' => 'ffff00', 'label' => __('Yellow')],
        ];
    }
}