<?php
/**
 * Copyright © 2016 Webinse. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webinse\CalendarEvents\Model\Config\Source;
use \Magento\Framework\Option\ArrayInterface;

class DayName implements ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('Abbreviation')],
            ['value' => 2, 'label' => __('Narrow')],
            ['value' => 3, 'label' => __('Wide')],
        ];
    }
}
