<?php
/**
 * Copyright © 2016 Webinse. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webinse\CalendarEvents\Model\Config\Source;
use \Magento\Framework\Option\ArrayInterface;

class FirstDay implements ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Sunday')],
            ['value' => 1, 'label' => __('Monday')],
        ];
    }
}
