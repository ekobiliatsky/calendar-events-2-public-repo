<?php
namespace Webinse\CalendarEvents\Controller\Index;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Customer\Model\Session as Session;

class Send extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;

    protected $customerSession;

    /**
     * Block factory
     *
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $_blockFactory;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Session $customerSession,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->pageFactory = $pageFactory;
        $this->customerSession = $customerSession;
        $this->_blockFactory = $blockFactory;
        $this->_storeManager = $storeManager;
        return parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $cmsBlockId = $this->customerSession->getData('id');
        //$this->_view->loadLayout();
        //$this->_view->getLayout()->getBlock('content')->createBlock();
        //$block = $this->_view->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($cmsBlockId);
        $block = $this->_blockFactory->create();
        $storeId = $this->_storeManager->getStore()->getId();
        $block->setStoreId($storeId)->load($cmsBlockId);
        $body = "Your firend ".$params['name']." just sent you an invitation for event: "
            .$block->getTitle()
            .", with following message: \r\n".$params['msg']."\r\n"
            .'Check out this awesome event: '. $this->_storeManager->getStore()->getUrl('calendar_events/show/event', array('id' => $cmsBlockId));

        $mail = new \Zend_Mail();
        //new \Zend_Mail();
        $mail->setBodyText($body);
        $mail->setFrom($params['email'], $params['name']);
        $mail->addTo($params['fremail'], 'Friend');
        $mail->setSubject('Invite to Event');
        try{
            $mail->send();
        }catch(Exception $e){
            Mage::getSingleton('core/session')->addError('Unable to send email.');
        }
    }    
}