<?php
namespace Webinse\CalendarEvents\Controller\Index;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\UrlInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param UrlInterface $url
     * @param Session $customerSession
     */
    public function __construct(Context $context, PageFactory $pageFactory, UrlInterface $url, Session $customerSession)
    {
        $this->pageFactory = $pageFactory;
        $this->url = $url;
        $this->session = $customerSession;
        return parent::__construct($context);
    }

    public function execute()
    {        
        //var_dump(__METHOD__);
        if ($this->session->isLoggedIn()) {
            $page_object = $this->pageFactory->create();

            return $page_object;
        } else {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->url->getUrl('customer/account'));

            return $resultRedirect;
        }
    }    
}