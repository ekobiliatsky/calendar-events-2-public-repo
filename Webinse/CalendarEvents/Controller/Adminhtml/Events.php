<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/3/16
 * Time: 2:43 PM
 */
namespace Webinse\CalendarEvents\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Webinse\CalendarEvents\Model\EventsFactory;

class Events extends Action
{
    public function execute(){}
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Result page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Events model factory
     *
     * @var \Webinse\CalendarEvents\Model\EventsFactory
     */
    protected $_eventsFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param EventsFactory $eventsFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        EventsFactory $eventsFactory
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_eventsFactory = $eventsFactory;
    }

    /**
     * News access rights checking
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webinse_CalendarEvents::manage_events');
    }
}