<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 6:29 PM
 */
namespace Webinse\CalendarEvents\Controller\Adminhtml\Events;

use Webinse\CalendarEvents\Controller\Adminhtml\Events;

class Edit extends Events
{
    /**
     * @return void
     */
    public function execute()
    {
        $eventsId = $this->getRequest()->getParam('id');
        /** @var \Webinse\CalendarEvents\Model\Events $model */
        $model = $this->_eventsFactory->create();

        if ($eventsId) {
            $model->load($eventsId);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This events no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        // Restore previously entered form data from session
        $data = $this->_session->getEventsData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('calendarevents_events', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Webinse_CalendarEvents::main_menu');
        $resultPage->getConfig()->getTitle()->prepend(__('Calendar Events'));

        return $resultPage;
    }
}