<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 6:33 PM
 */
namespace Webinse\CalendarEvents\Controller\Adminhtml\Events;

use Webinse\CalendarEvents\Controller\Adminhtml\Events;

class Save extends Events
{
    /**
     * @return void
     */
    public function execute()
    {
        $isPost = $this->getRequest()->getPost();

        if ($isPost) {
            $eventsModel = $this->_eventsFactory->create();
            $Id = $this->getRequest()->getParam('id');

            if ($Id) {
                $eventsModel->load($Id);
            }
            $formData = $this->getRequest()->getParam('events');
            $eventsModel->setData($formData);
            $eventsModel->setAssignGroup(implode($formData['assign_group'], ','));

            try {
                // Save events
                $eventsModel->save();
                $this->_objectManager->create('Webinse\CalendarEvents\Helper\Data')
                    ->sendMail($eventsModel->getData(), $eventsModel->isObjectNew());
                // Display success message
                $this->messageManager->addSuccess(__('The events has been saved. Email successfully sent'));

                // Check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $eventsModel->getId(), '_current' => true]);
                    return;
                }

                // Go to grid page
                $this->_redirect('*/*/');
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }

            $this->_getSession()->setFormData($formData);
            $this->_redirect('*/*/edit', ['id' => $Id]);
        }
    }
}