<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 6:40 PM
 */
namespace Webinse\CalendarEvents\Controller\Adminhtml\Events;

use Webinse\CalendarEvents\Controller\Adminhtml\Events;

class MassDelete extends Events
{
    /**
     * @return void
     */
    public function execute()
    {
        // Get IDs of the selected events
        $Ids = $this->getRequest()->getParam('events');

        foreach ($Ids as $Id) {
            try {
                /** @var $eventsModel \Webinse\CalendarEvents\Model\Events */
                $eventsModel = $this->_eventsFactory->create();
                $eventsModel->load($Id)->delete();
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if (count($Ids)) {
            $this->messageManager->addSuccess(
                __('A total of %1 event(s) were deleted.', count($Ids))
            );
        }

        $this->_redirect('*/*/index');
    }
}