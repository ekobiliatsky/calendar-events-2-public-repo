<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/3/16
 * Time: 2:51 PM
 */
namespace Webinse\CalendarEvents\Controller\Adminhtml\Events;

use Webinse\CalendarEvents\Controller\Adminhtml\Events;

class Index extends Events
{
    /**
     * @return void
     */
    public function execute()
    {
        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Webinse_CalendarEvents::main_menu');
        $resultPage->getConfig()->getTitle()->prepend(__('Calendar Events'));

        return $resultPage;
    }
}