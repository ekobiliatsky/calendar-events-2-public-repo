<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/3/16
 * Time: 2:52 PM
 */
namespace Webinse\CalendarEvents\Controller\Adminhtml\Events;

use Webinse\CalendarEvents\Controller\Adminhtml\Events;

class Grid extends Events
{
    /**
     * @return void
     */
    public function execute()
    {
        return $this->_resultPageFactory->create();
    }
}