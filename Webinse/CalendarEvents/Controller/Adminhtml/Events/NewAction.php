<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 6:28 PM
 */

namespace Webinse\CalendarEvents\Controller\Adminhtml\Events;

use Webinse\CalendarEvents\Controller\Adminhtml\Events;

class NewAction extends Events
{
    /**
     * Create new events action
     *
     * @return void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}