<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/2/16
 * Time: 6:36 PM
 */
namespace Webinse\CalendarEvents\Controller\Adminhtml\Events;

use Webinse\CalendarEvents\Controller\Adminhtml\Events;

class Delete extends Events
{
    /**
     * @return void
     */
    public function execute()
    {
        $Id = (int) $this->getRequest()->getParam('id');

        if ($Id) {
            /** @var $eventsModel \Webinse\CalendarEvents\Model\Events */
            $eventsModel = $this->_eventsFactory->create();
            $eventsModel->load($Id);

            // Check this news exists or not
            if (!$eventsModel->getId()) {
                $this->messageManager->addError(__('This events no longer exists.'));
            } else {
                try {
                    // Delete events
                    $eventsModel->delete();
                    $this->messageManager->addSuccess(__('The events has been deleted.'));

                    // Redirect to grid page
                    $this->_redirect('*/*/');
                    return;
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                    $this->_redirect('*/*/edit', ['id' => $eventsModel->getId()]);
                }
            }
        }
    }
}