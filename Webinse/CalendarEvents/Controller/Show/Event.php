<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/4/16
 * Time: 2:43 PM
 */
namespace Webinse\CalendarEvents\Controller\Show;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Customer\Model\Session;

class Event extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $catalogSession;

    public function __construct(Context $context, PageFactory $pageFactory, \Magento\Customer\Model\Session $catalogSession)
    {
        $this->pageFactory = $pageFactory;
        $this->catalogSession = $catalogSession;
        return parent::__construct($context);
    }

    public function execute()
    {
        //var_dump(__METHOD__);
        $blockId = $this->getRequest()->getParam('id');
        $this->catalogSession->setData('id', $blockId);
        $page_object = $this->pageFactory->create();
        return $page_object;
    }
    /**
     * Show Contact Us page
     *
     * @return void
     */
    /*public function execute()
    {
        $this->_view->loadLayout();
        //$this->_view->getLayout()->getBlock('content')->createBlock();
        $block = $this->_view->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId('contact-us-info')->toHtml();

        //$this->_view->renderLayout();
    }*/
}