<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/4/16
 * Time: 3:53 PM
 */
namespace Webinse\CalendarEvents\Block;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Webinse\CalendarEvents\Helper\Data;
use \Magento\Framework\UrlInterface;

class Show extends Template
{
    /**
     * @param Context $context
     * @param Data $helper
     * @param UrlInterface $urlinterface
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helper,
        UrlInterface $urlinterface,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->_urlinterface = $urlinterface;
        parent::__construct($context, $data);

    }

    protected function _prepareLayout()
    {

    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCmsBlock()
    {
        $cmsBlockId = $this->getRequest()->getParam('id');
        $block = $this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($cmsBlockId)->toHtml();

        return $block;
    }

    /**
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->_urlinterface->getCurrentUrl();
    }
}
