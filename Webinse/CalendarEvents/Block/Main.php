<?php
namespace Webinse\CalendarEvents\Block;
use Magento\Framework\View\Element\Template;
use Webinse\CalendarEvents\Model\EventsFactory;
use \Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template\Context;
use Webinse\CalendarEvents\Helper\Data;
class Main extends Template
{
    /**
    * @var \Webinse\CalendarEvents\Model\EventsFactory
    */
	protected $_modelEventsFactory;

    /**
     * @param Context $context
     * @param Data $helper
     * @param EventsFactory $modelEventsFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helper,
        EventsFactory $modelEventsFactory,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->_modelEventsFactory = $modelEventsFactory;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
    
    }

    /**sort Cms pages by Allowed User groups
     * prepare data for Event json transfer
     * @return Json string
     */
    public function getJsonEvents(){
        $eventsModel = $this->_modelEventsFactory->create();
        $eventsCollection = $eventsModel->getCollection();
        $blocks = $eventsCollection->getItems();
        $events = array();
        foreach($blocks as $block){
            $group = str_split(intval(preg_replace('/[^\d.]/', '',$block->getAssignGroup())));
            $objectManager = ObjectManager::getInstance();
            $customer = $objectManager->create('Magento\Customer\Model\Session');
            if ($customer->isLoggedIn()) {
                if(is_array($group) && in_array($customer->getCustomerGroupId(), $group)){
                    $e = array();
                    $e['title']  = $block->getTitle();
                    $e['url']    = $this->getUrl('calendar_events/show/event', array('id' => $block->getIdentifier()));
                    $e['start']  = $block->getEventDateFrom();
                    $e['end']    = $block->getEventDateTo();
                    $e['allDay'] = 'true';
                    array_push($events, $e);
                }
            }
        }

        return json_encode($events);
    }
}
