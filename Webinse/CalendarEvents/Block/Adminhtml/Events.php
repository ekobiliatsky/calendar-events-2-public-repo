<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 2/3/16
 * Time: 2:40 PM
 */

namespace Webinse\CalendarEvents\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Events extends Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_events';
        $this->_blockGroup = 'Webinse_CalendarEvents';
        $this->_headerText = __('Manage Events');
        $this->_addButtonLabel = __('Add Events');
        parent::_construct();
    }
}